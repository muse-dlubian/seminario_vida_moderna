import numpy as np
from scipy.special import jv
import scipy.integrate as spint

I = spint.quad(lambda x: jv(2.5, x), 0, 4.5)

def fun(x):
    return jv(2.5, x)
